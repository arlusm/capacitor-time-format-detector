package ee.lusmagi;

import android.text.format.DateFormat;
import com.getcapacitor.JSObject;
import com.getcapacitor.Plugin;
import com.getcapacitor.PluginCall;
import com.getcapacitor.PluginMethod;
import com.getcapacitor.annotation.CapacitorPlugin;

@CapacitorPlugin(name = "CapacitorTimeFormatDetector")
public class CapacitorTimeFormatDetectorPlugin extends Plugin {

    private CapacitorTimeFormatDetector implementation = new CapacitorTimeFormatDetector();

    @PluginMethod
    public void isSystem24HourFormat(PluginCall call) {
        boolean is24Hour = DateFormat.is24HourFormat(getContext());
        JSObject ret = new JSObject();
        ret.put("is24Hour", is24Hour);
        call.resolve(ret);
    }
}
