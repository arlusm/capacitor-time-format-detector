# capacitor-time-format-detector

Determine if system is using 24h or 12h time format

## Install

```bash
npm install capacitor-time-format-detector
npx cap sync
```

## API

<docgen-index>

* [`isSystem24HourFormat(...)`](#isSystem24HourFormat)

</docgen-index>

<docgen-api>
<!--Update the source file JSDoc comments and rerun docgen to update the docs below-->

### isSystem24HourFormat(...)

```typescript
isSystem24HourFormat() => Promise<{ is24Hour: boolean; }>
```

**Returns:** <code>Promise&lt;{ is24Hour: boolean; }&gt;</code>

--------------------

</docgen-api>
