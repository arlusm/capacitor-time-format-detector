import { registerPlugin } from '@capacitor/core';

import type { CapacitorTimeFormatDetectorPlugin } from './definitions';

const CapacitorTimeFormatDetector =
  registerPlugin<CapacitorTimeFormatDetectorPlugin>(
    'CapacitorTimeFormatDetector',
    {
      web: () => import('./web').then(m => new m.CapacitorTimeFormatDetectorWeb()),
    },
  );

export * from './definitions';
export { CapacitorTimeFormatDetector };
