export interface CapacitorTimeFormatDetectorPlugin {
  isSystem24HourFormat(): Promise<{ is24Hour: boolean }>;
}
