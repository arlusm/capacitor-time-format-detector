import { WebPlugin } from '@capacitor/core';

import type { CapacitorTimeFormatDetectorPlugin } from './definitions';

export class CapacitorTimeFormatDetectorWeb
  extends WebPlugin
  implements CapacitorTimeFormatDetectorPlugin
{
  isSystem24HourFormat(): Promise<{ is24Hour: boolean }> {
    return Promise.resolve({is24Hour: false});
  }
}
