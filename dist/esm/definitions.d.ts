export interface CapacitorTimeFormatDetectorPlugin {
    echo(options: {
        value: string;
    }): Promise<{
        value: string;
    }>;
}
