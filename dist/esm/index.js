import { registerPlugin } from '@capacitor/core';
const CapacitorTimeFormatDetector = registerPlugin('CapacitorTimeFormatDetector', {
    web: () => import('./web').then(m => new m.CapacitorTimeFormatDetectorWeb()),
});
export * from './definitions';
export { CapacitorTimeFormatDetector };
//# sourceMappingURL=index.js.map