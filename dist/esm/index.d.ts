import type { CapacitorTimeFormatDetectorPlugin } from './definitions';
declare const CapacitorTimeFormatDetector: CapacitorTimeFormatDetectorPlugin;
export * from './definitions';
export { CapacitorTimeFormatDetector };
