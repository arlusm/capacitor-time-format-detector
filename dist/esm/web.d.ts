import { WebPlugin } from '@capacitor/core';
import type { CapacitorTimeFormatDetectorPlugin } from './definitions';
export declare class CapacitorTimeFormatDetectorWeb extends WebPlugin implements CapacitorTimeFormatDetectorPlugin {
    echo(options: {
        value: string;
    }): Promise<{
        value: string;
    }>;
}
