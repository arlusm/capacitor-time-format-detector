'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var core = require('@capacitor/core');

const CapacitorTimeFormatDetector = core.registerPlugin('CapacitorTimeFormatDetector', {
    web: () => Promise.resolve().then(function () { return web; }).then(m => new m.CapacitorTimeFormatDetectorWeb()),
});

class CapacitorTimeFormatDetectorWeb extends core.WebPlugin {
    async echo(options) {
        console.log('ECHO', options);
        return options;
    }
}

var web = /*#__PURE__*/Object.freeze({
    __proto__: null,
    CapacitorTimeFormatDetectorWeb: CapacitorTimeFormatDetectorWeb
});

exports.CapacitorTimeFormatDetector = CapacitorTimeFormatDetector;
//# sourceMappingURL=plugin.cjs.js.map
