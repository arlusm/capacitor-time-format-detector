var capacitorCapacitorTimeFormatDetector = (function (exports, core) {
    'use strict';

    const CapacitorTimeFormatDetector = core.registerPlugin('CapacitorTimeFormatDetector', {
        web: () => Promise.resolve().then(function () { return web; }).then(m => new m.CapacitorTimeFormatDetectorWeb()),
    });

    class CapacitorTimeFormatDetectorWeb extends core.WebPlugin {
        async echo(options) {
            console.log('ECHO', options);
            return options;
        }
    }

    var web = /*#__PURE__*/Object.freeze({
        __proto__: null,
        CapacitorTimeFormatDetectorWeb: CapacitorTimeFormatDetectorWeb
    });

    exports.CapacitorTimeFormatDetector = CapacitorTimeFormatDetector;

    Object.defineProperty(exports, '__esModule', { value: true });

    return exports;

})({}, capacitorExports);
//# sourceMappingURL=plugin.js.map
