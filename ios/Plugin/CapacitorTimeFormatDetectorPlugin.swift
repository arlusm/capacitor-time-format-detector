import Foundation
import Capacitor

/**
 * Please read the Capacitor iOS Plugin Development Guide
 * here: https://capacitorjs.com/docs/plugins/ios
 */
@objc(CapacitorTimeFormatDetectorPlugin)
public class CapacitorTimeFormatDetectorPlugin: CAPPlugin {
     @objc func isSystem24HourFormat(_ call: CAPPluginCall) {
        let formatString = DateFormatter.dateFormat(fromTemplate: "j", options: 0, locale: Locale.current)
        let is24Hour = !formatString!.contains("a")
        call.resolve(["is24Hour": is24Hour])
    }
}
